# OneDayCarRental.biz Quote Me!

_OneDayCarRental.biz_. is a Docker Swarm that is load-balanced using a Nginx gRPC and Html reverse-proxy.

When someone queries the site they will first hit the frontend gateway via port 80. Nginx then passes the request upstream 
to a _onedaycarrental_ service. The _onedaycarrental_ service then requests gRPC calls from both _rentalcompanya_ and _rentalcompanyb_
via the gRPC reverse-proxy. 

Load balancing is configured using a _round robbin_ strategy and will resolve each service one by one to spread load. 
This allows the swarm to scale.

Current scale configuration:

 - _rentalcompanya_ = 3 (more complex than b - might need scale)
 - _rentalcompanyb_ = 3 
 - _onedaycarrental_ = 3 (handles more requests)
 - _nginx_ = 1 

This is a per node setup in Docker Swarm.

## Docker Swarm

The setup in the _docker-compose.yml_ file outlines four services. There is a single _Nginx_ instance which has not yet 
been configured for redundancy. All other services otherwise have a very similar setup. 
which includes allowances for replica instances, restart policies and some basic resource control. 

## Contents

There are three services in this repository:

 1. `rentalcompanya` (./odcr/usvc/rentalcompanya)
 2. `rentalcompanyb` (./odcr/usvc/rentalcompanyb)
 3. `onedaycarrental` (./odcr/web/onedaycarrental)

_rentalcompanya_ and _rentalcompanyb_ both define their own gRPC service however they share the same base implementation 
(odcr/lib/service/factory.go)

_onedaycarrental_ is a web handler and router which calls both of the rental company clients and combining their responses.

### Let's try this

All containers are pre built and ready to go! If for some reason you cannot access the docker containers you can build 
them manually via the Makefile's.

[Makefile]

```makefile
make nginx-cert
```

[web/onedaycarrental/Makefile]

```makefile
make docker-all
```

[usvc/rentalcompanya/Makefile]

```makefile
make docker-all
```

[usvc/rentalcompanyb/Makefile]

```makefile
make docker-all
```

Yuu can then run the Docker Swarm.

[BitBucket](https://bitbucket.org/twinj/odcr/src/master/)

Clone this repo or get a copy of the _docker-compose.yml_ file. Make sure you hav the latest version of _docker_ 

I used these tools for this challenge:
 - Docker `Version 2.0.0.2 (30215)`
 - macOS Mojave `Version 10.14 (18A391)`
 
 ## Start the swarm

First your machine is setup as a manager node.

```bash
docker swarm init
```

Ensure the stack name is `odcr` or this wont work. The certificates and docker-compose expect this.

```bash
docker stack deploy -c docker-compose.yml odcr
```

Let us now have a look at how many containers were created.

You should now be able to hit:
 
 - `GetQuotes` [http://localhost/quotes]
 - `GetQuotesJSON` [http://localhost/quotes/json]
 
 The routes are basic but get the job done.
 
 - `GetQuotes` shows a html template which is generated on each request us the std golang _html/template_ lib.
 - `GetQuotesJSON` returns the raw JSON with a few spaces for readability.
 
```bash
docker container ls
```

### Shut down swarm

```bash
docker swarm leave -f 
```

### ⚡ Scale examples

```bash
docker service scale odcr_onedaycarrental=4
docker service scale odcr_rentalcompanya=3
docker service scale odcr_rentalcompanyb=3
 
```

### Create load

Using zsh or bash.

```bash
repeat 10000 { curl localhost/quotes >/dev/null; }

for ((n=0;n<10;n++)); do curl localhost:4000; echo; done
```

### Example certificate generation

```bash
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ./cert/nginx.key -out ./cert/nginx.cert  -subj '/CN=odcr_nginx'
```
