package main

import (
	"bitbucket.org/twinj/odcr/lib/odcr"
	"bitbucket.org/twinj/odcr/lib/service"
	"bitbucket.org/twinj/odcr/usvc/rentalcompanyb/protobuf/rcb"
	"flag"
	"fmt"
	"google.golang.org/grpc"
	"log"
	"net"
	"os"
)

const (
	name = "rentalcompanyb"
)

var (
	port              = "11112"
	tls               bool
	certFile, keyFile string
)

func init() {
	flag.BoolVar(&tls, "tls", true, "Connection uses TLS if true, else plain TCP")

	certFile = os.Getenv("SSL_CERT")
	keyFile = os.Getenv("SSL_KEY")
	if env, ok := os.LookupEnv("PORT"); ok {
		port = env
	}

	flag.Parse()
}

func main() {
	var opts []grpc.ServerOption
	if tls {
		opt, err := odcr.ReadCertificatesForServer(certFile, keyFile)
		if err != nil {
			log.Fatalf("failed to get credentials: %v", err)
		}
		opts = append(opts, opt)
	}

	_, entry := odcr.NewLogger(name, true)
	gs := odcr.NewServer(entry, opts...)

	s := service.GetQuoteFuncFactory(entry, "Service B Rental", "AUD", 30)

	rcb.RegisterQuoterServiceServer(gs, s)
	entry.Info("Service registered")

	lis, err := net.Listen("tcp", fmt.Sprintf(":%s", port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	if err := gs.Serve(lis); err != nil {
		log.Fatalf("failed to start service: %v", err)
	}
}
