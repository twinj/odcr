# rentalcompanyb

The service is an implementation of the gRPC `rcb.QuoterServiceServer` interface. 
It instantiates a standalone service.Quoter and passes in a factory method to satisfy the requirement for the service.GetQuoteFunc. 

It returns static data to any caller as implemented by the service.GetQuoteFuncFactory.

### Build

To do a normal build use either `go` or `make`
```bash
go build -v -tags=debug
```

```bash
make build
```

There is one build tags available using the -tags option in go build.

- `debug` this will turn on debug logging for gateway marshalling.

### Cross compile for docker
```makefile
make build-linux
make build-docker

```


### Run

By default this service will run on port `11112` with tls enabled. If running locally remember to generate a certificate 
with the right name.

### Command args

```bash
rentalcompanyb -tls=false
```

### ENV VARS

SSL_CERT: sets the cert filename location to use - required when `-tls=true`
SSL_KEY: sets the keys filename location to use - required when `-tls=true` 
PORT: sets the port to use

Typically the certificate files you would use are at odcr/cert
