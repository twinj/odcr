// +build testhttp

package main

import (
	"io"
	"net/http"
	"net/http/httptest"
	"bitbucket.org/twinj/odcr/lib/gateway/rentalcompanya"
)

var (
	server = httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		io.WriteString(w, `{"quote":{"name":"Service A Rental","amount":"55.00","currency":"AUD"}}`)
	}))

	options = []rentalcompanya.Option{
		rentalcompanya.SetEndpointURL(server.URL),
	}
)
