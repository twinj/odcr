# rentalcompanya

The service is an implementation of the gRPC `rca.QuoterServiceServer` interface. 
It instantiates a standalone gateway caller rentalcompanya.Gateway to satisfy the requirement for the `service.GetQuoteFunc`. 
The gateway GetQuote call is adapted into a `service.GetQuoteFunc` which satisfies the interface.

Calls "https://api.myjson.com/bins/7c0qw" to get quotes and returns some JSON:
```json
{
  "quote": {
    "name": "Service A",
    "amount": "55.00",
    "currency": "AUD"
  }
}
```

### Build

To do a normal build use either `go` or `make`
```bash
go build -v -tags=debug,httptest
```

```bash
make build
```

There are two build tags available using the -tags option in go build.

- `debug` this will turn on debug logging for gateway marshalling.
- `httptest` this will turn on httptest clients instead of calling the real service.

### Cross compile for docker
```makefile
make build-linux
make build-docker

```

### Run

By default this service will run on port `11111` with tls enabled. If running locally remember to generate a certificate 
with the right name.

### Command args

```bash
rentalcompanya -tls=false
```

### ENV VARS

SSL_CERT: sets the cert filename location to use - required when `-tls=true`
SSL_KEY: sets the keys filename location to use - required when `-tls=true` 
PORT: sets the port to use

Typically the certificate files you would use are at odcr/cert
