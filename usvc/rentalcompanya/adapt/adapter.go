// Package adapt will create an adaptor function to convert a gateway.GetQuote response
// to a pquoter.QuoterClient GetQuote response.
// The purpose of this is to decouple the gateway library from the pquoter.QuoterService library.
// Even though the types are the same now we may need to extract out the gateway service in the future
package adapt

import (
	"bitbucket.org/twinj/odcr/lib/gateway/rentalcompanya"
	"bitbucket.org/twinj/odcr/lib/service"
	"bitbucket.org/twinj/odcr/protobuf/ptypes/rentals"
	"context"
	"github.com/sirupsen/logrus"
)

// ToGetQuoteFunc uses the factory pattern to adapt the gateway caller to the quoter function required by the quoter service.
func ToGetQuoteFunc(logger logrus.FieldLogger, gw rentalcompanya.Gateway) service.GetQuoteFunc {
	logger.Info("Gateway quoter adapter created")
	return func(ctx context.Context) (*rentals.Quote, error) {
		quote, err := gw.GetQuote(ctx)
		if err != nil {
			return nil, err
		}

		return &rentals.Quote{
			Name:     quote.Name,
			Amount:   quote.Amount,
			Currency: quote.Currency,
		}, nil
	}
}
