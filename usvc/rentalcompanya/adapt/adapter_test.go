package adapt

import (
	"bitbucket.org/twinj/odcr/lib/gateway/rentalcompanya"
	"context"
	"github.com/sirupsen/logrus"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

func TestToGetQuoteFunc(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		io.WriteString(w, `{"quote":{"name":"XYZ Rental","amount":"45.00","currency":"USD"}}`)
	}))
	defer server.Close()

	gw, err := rentalcompanya.NewGateway(
		logrus.New(),
		rentalcompanya.SetEndpointURL(server.URL),
		rentalcompanya.SetHTTPClient(
			&http.Client{
				Timeout: time.Second,
			},
		),
	)
	if err != nil {
		t.Fatalf("There should be no error but got %v", err)
	}

	q, err := ToGetQuoteFunc(
		logrus.New(),
		gw)(context.Background())
	if err != nil {
		t.Errorf("There should be no error but got %v", err)
	}

	if q == nil {
		t.Error("The quote should exist")
	}

	if q.Name != "XYZ Rental" {
		t.Errorf("The name should be [XYZ Rental] but got [%s]", q.Name)
	}

	if q.Currency != "USD" {
		t.Errorf("The currency should be [USD] but got [%s]", q.Currency)
	}

	const quote = 45.0

	if q.Amount != quote {
		t.Errorf("The quote should be [%.2f] but got [%.2f]", quote, q.Amount)
	}
}
