// +build !testhttp

package main

import (
	"bitbucket.org/twinj/odcr/lib/gateway/rentalcompanya"
	ctls "crypto/tls"
	"net/http"
	"time"
)

var (
	// creating the client with InsecureSkipVerify: true was not an option
	// as the endpoint otherwise fails
	defaultClient = &http.Client{
		Timeout: time.Second * 60,
		Transport: &http.Transport{
			TLSClientConfig: &ctls.Config{
				MinVersion:         ctls.VersionTLS12,
				InsecureSkipVerify: true,
			},
		},
	}

	options = []rentalcompanya.Option{
		rentalcompanya.SetHTTPClient(defaultClient),
	}
)
