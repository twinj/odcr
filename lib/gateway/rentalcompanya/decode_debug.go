// +build debug

package rentalcompanya

import (
	"bytes"
	"encoding/json"
	"github.com/y0ssar1an/q"
	"io"
	"io/ioutil"
)

func decodeJSON(body io.Reader, rsp interface{}) error {
	by, err := ioutil.ReadAll(body)
	if err != nil {
		return err
	}

	q.Q(string(by))

	return json.NewDecoder(bytes.NewReader(by)).Decode(rsp)
}