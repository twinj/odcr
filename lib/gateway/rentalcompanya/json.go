package rentalcompanya

import (
	"io"
)

// Quote represents the RentalCompanyAs endpoint quote.
// {"quote":{"name":"Service A Rental","amount":"55.00","currency":"AUD"}}
type Quote struct {
	Name     string  `json:"name"`
	Amount   float64 `json:"amount,string"`
	Currency string  `json:"currency"`
}

func decodeQuote(body io.Reader) (*Quote, error) {
	quote := &Quote{}

	if err := decodeJSON(body, &struct {
		*Quote `json:"quote"`
	}{quote}); err != nil {
		return nil, err
	}

	return quote, nil
}
