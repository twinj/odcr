// +build !debug

package rentalcompanya

// https://dave.cheney.net/2014/09/28/using-build-to-switch-between-debug-and-release

import (
	"encoding/json"
	"io"
)

func decodeJSON(body io.Reader, rsp interface{}) error {
	return json.NewDecoder(body).Decode(rsp);
}
