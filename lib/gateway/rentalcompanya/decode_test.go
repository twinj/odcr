package rentalcompanya

import (
	"bytes"
	"encoding/json"
	"testing"
)

func Test_decodeJSON(t *testing.T) {
	const rsp = `{"quote":{"name":"Service A Rental","amount":"55.00","currency":"AUD"}}`
	if err := decodeJSON(bytes.NewReader([]byte(rsp)), &json.RawMessage{}); err != nil {
		t.Errorf("There should be no error but got %v", err)
	}
}