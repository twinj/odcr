package rentalcompanya

import (
	"context"
	"github.com/sirupsen/logrus"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

func TestNewGateway(t *testing.T) {
	gw, err := NewGateway(logrus.New(),
	)
	if err != nil {
		t.Errorf("There should be no error but got %v", err)
	}

	if gw == nil {
		t.Error("The gateway service should exist")
	}
}

func TestGateway_GetQuote(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		io.WriteString(w, `{"quote":{"name":"Service A","amount":"55.00","currency":"AUD"}}`)
	}))
	defer server.Close()

	gw, err := NewGateway(
		logrus.New(),
		SetEndpointURL(server.URL),
		SetHTTPClient(&http.Client{
		Timeout: time.Second,
	}))
	if err != nil {
		t.Fatalf("There should be no error but got %v", err)
	}

	// Call the gateway and get a quote
	q, err := gw.GetQuote(context.Background())
	if err != nil {
		t.Errorf("There should be no error but got %v", err)
	}

	if q == nil {
		t.Error("the quote should exist")
	}

	if q.Name != "Service A" {
		t.Errorf("The name should be [Service A] but got [%s]", q.Name)
	}

	if q.Currency != "AUD" {
		t.Errorf("The currency should be [AUD] but got [%s]", q.Currency)
	}

	const quote = 55.0

	if q.Amount != quote {
		t.Errorf("The quote should be [%.2f] but got [%.2f]", quote, q.Amount)
	}
}
