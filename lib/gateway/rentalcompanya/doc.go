// Package rentalcompanya allows one to make calls to the RentalCompanyA's API.
//
// It is designed to be separate from all other architecture or services so as to separate concerns,
// and improve testability and maintainability. There are options to build this using debug build tags
// so that you can use the q.Q logger to output any responses received from the caller. Th elogger is intended to aid in
// development
//
// While the url endpoint is hardcoded you can pass an Option to change this setting. This helps passing in a custom
// test server to perform integration testing and to ensure that there is an option not to connect to a production
// environment.
package rentalcompanya
