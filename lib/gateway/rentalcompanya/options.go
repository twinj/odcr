package rentalcompanya

import "net/http"

// Options allows the gateway client to be setup in your own way.
type Options struct {
	client   *http.Client
	endpoint string
}

// Option sets up any option available in the gateway.
type Option func(*Options)

// SetHTTPClient sets the http client on the gateway if passed as an option to NewGateway.
func SetHTTPClient(client *http.Client) Option {
	return func(opts *Options) {
		opts.client = client
	}
}

// SetEndpointURL sets the http client requests URL on the gateway if passed as an option to NewGateway.
func SetEndpointURL(endpoint string) Option {
	return func(opts *Options) {
		opts.endpoint = endpoint
	}
}
