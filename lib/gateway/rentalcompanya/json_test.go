package rentalcompanya

import (
	"bytes"
	"testing"
)

func Test_decodeQuote(t *testing.T) {
	q, err := decodeQuote(bytes.NewReader([]byte(`{"quote":{"name":"Service A","amount":"59.00","currency":"USD"}}`)))
	if err != nil {
		t.Errorf("There should be no error but got %v", err)
	}

	if q == nil {
		t.Error("The quote should exist")
	}

	if q.Name != "Service A" {
		t.Errorf("The name should be [Service A] but got [%s]", q.Name)
	}

	if q.Currency != "USD" {
		t.Errorf("The currency should be [USD] but got [%s]", q.Currency)
	}

	const quote = 59.0

	if q.Amount != quote {
		t.Errorf("The quote should be [%.2f] but got [%.2f]", quote, q.Amount)
	}
}
