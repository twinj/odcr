package rentalcompanya

import (
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
	"net/http"
	"net/url"
	"time"
)

// Gateway implements any calls available from the RentalCompanyA API.
type Gateway interface {
	// GetQuote makes a http request to the company gateway and returns a quote.
	GetQuote(ctx context.Context) (*Quote, error)
}

type gateway struct {
	client   *http.Client
	endpoint *url.URL
	logger   logrus.FieldLogger
}

func (g *gateway) GetQuote(ctx context.Context) (*Quote, error) {
	g.logger.Debug("Getting quote from gateway")

	// It is normally more efficient to extract out pure http logic from a g
	req, err := http.NewRequest(http.MethodGet, g.endpoint.String(), nil)
	if err != nil {
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")
	req.Close = true
	req.WithContext(ctx)

	rsp, err := g.client.Do(req)
	if err != nil {
		return nil, err
	}

	defer rsp.Body.Close()

	if http.StatusOK != rsp.StatusCode {
		err = fmt.Errorf("%d - %s", rsp.StatusCode, rsp.Status)
		return nil, err
	}

	g.logger.Debug("Decoding response from gateway")
	return decodeQuote(rsp.Body)
}

// NewGateway returns a ready to use quote caller for RentalCompanyA. It creates a basic http.Client with a 30 second
// timeout by default. The gateway hardcodes the endpoint url to connect with RentalCompanyB's API directly.
// For tests and mocking pass in a httptest.Server url.
func NewGateway(logger logrus.FieldLogger, options ...Option) (Gateway, error) {
	opts := &Options{
		client: &http.Client{
			Timeout: 30 * time.Second,
		},
		endpoint: "https://api.myjson.com/bins/7c0qw",
	}

	for _, v := range options {
		v(opts)
	}

	ep, err := url.Parse(opts.endpoint)
	if err != nil {
		return nil, err
	}

	return &gateway{
		client:   opts.client,
		endpoint: ep,
		logger:   logger,
	}, nil
}
