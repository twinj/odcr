package service

import (
	"context"
	"github.com/sirupsen/logrus"
	"testing"
)

func TestQuoteFuncFactory(t *testing.T) {
	const quote = 30.0

	q, err := GetQuoteFuncFactory(logrus.New(), "Service B Rental", "AUD", quote)(context.Background())
	if err != nil {
		t.Errorf("There should be no error but got %v", err)
	}

	if q.Name != "Service B Rental" {
		t.Errorf("The name should be [Service B Rental] but got [%s]", q.Name)
	}

	if q.Currency != "AUD" {
		t.Errorf("The currency should be [AUD] but got [%s]", q.Currency)
	}

	if q.Amount != quote {
		t.Errorf("The quote should be [%.2f] but got [%.2f]", quote, q.Amount)
	}
}

func TestGetQuoteFunc_GetQuote(t *testing.T) {
	log := logrus.New()
	for _, v := range []struct {
		amount, expected float64
		name, currency   string
	}{
		{30, 34.5, "Service 1", "NZD"},
		{1, 1.15, "Service 2", "AUD"},
		{100, 115, "Service 3", "AUD"},
		{30.50, 35.07, "Service 4", "AUD"},
		{30.501, 35.08, "Service 4", "AUD"},
		{100.01, 115.01, "Service XYZ", "USD"},
		{99.99, 114.99, "Service XYZ", "USD"},
		{99.999, 115, "Service JJ", "USD"},
		{100.0099, 115.01, "Service XYZ", "AUD"},
	} {
		q, err := GetQuoteFuncFactory(log, v.name, v.currency, v.amount).GetQuote(context.Background(), nil)
		if err != nil {
			t.Errorf("There should be no error but got %v", err)
		}

		if q.Name != v.name {
			t.Errorf("The name should be [%s] but got [%s]", v.name, q.Name)
		}

		if q.Currency != v.currency {
			t.Errorf("The currency should be [%s] but got [%s]", v.currency, q.Currency)
		}

		if q.Amount != v.expected {
			t.Errorf("The quote should be [%.2f] but got [%.2f]", v.expected, q.Amount)
		}
	}
}
