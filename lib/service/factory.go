// Package service implements the rca.QuoterServiceServer and rcb.QuoterServiceServer interfaces.
// It defines a GetQuoteFunc with which you can use to to satisfy the required interface.
// The GetQuoteFuncFactory returns a static version.
package service

import (
	"bitbucket.org/twinj/odcr/protobuf/ptypes/rentals"
	"context"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"math"
)

// QuoterServiceClient is the client API for any QuoterService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type QuoterServiceClient interface {
	// GetQuote returns a quote amount from the rental company.
	GetQuote(ctx context.Context, in *empty.Empty, opts ...grpc.CallOption) (*rentals.Quote, error)
}

// GetQuoteFunc is a function which can be passed as a pquoter.QuoterServer as an implementation for the GetQuote call.
type GetQuoteFunc func(ctx context.Context) (*rentals.Quote, error)

// GetQuote satisfies the pquoter.QuoterServer interface.
func (s GetQuoteFunc) GetQuote(ctx context.Context, em *empty.Empty) (*rentals.Quote, error) {
	q, err := s(ctx)
	if err != nil {
		return nil, err
	}
	q.Amount = addOurBit(q.Amount)
	return q, nil
}

// Add our bit and round to closest 2nd decimal place
func addOurBit(amount float64) float64 {
	return math.Round(amount*1.15*100) / 100
}

// GetQuoteFuncFactory returns a static service.GetQuoteFunc.
func GetQuoteFuncFactory(logger logrus.FieldLogger, name, currency string, amount float64) GetQuoteFunc {
	logger.WithFields(logrus.Fields{
		"name":     name,
		"currency": currency,
		"amount":   amount,
	}).Info("Static quoter factory created")
	return func(_ context.Context) (*rentals.Quote, error) {
		return &rentals.Quote{
			Name:     name,
			Amount:   amount,
			Currency: currency,
		}, nil
	}
}
