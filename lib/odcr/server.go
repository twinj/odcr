package odcr

import (
	"crypto/tls"
	"crypto/x509"
	"errors"
	"github.com/grpc-ecosystem/go-grpc-middleware"
	"github.com/grpc-ecosystem/go-grpc-middleware/logging/logrus"
	"github.com/grpc-ecosystem/go-grpc-middleware/tags"
	"github.com/minio/minio/cmd/logger"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials"
	"io/ioutil"
)

// NewServer returns a new grpc server with logging initialised.
func NewServer(entry *logrus.Entry, options ...grpc.ServerOption) *grpc.Server {
	logger.Info("Creating new service")

	opts := []grpc_logrus.Option{
		grpc_logrus.WithLevels(func(code codes.Code) logrus.Level {
			return logrus.GetLevel()
		}),
	}

	// Create a server, make sure we put the grpc_ctxtags context before everything else.
	gs := grpc.NewServer(
		append(options,
			grpc_middleware.WithUnaryServerChain(
				grpc_ctxtags.UnaryServerInterceptor(grpc_ctxtags.WithFieldExtractor(grpc_ctxtags.CodeGenRequestFieldExtractor)),
				grpc_logrus.UnaryServerInterceptor(entry, opts...),
			),
			grpc_middleware.WithStreamServerChain(
				grpc_ctxtags.StreamServerInterceptor(grpc_ctxtags.WithFieldExtractor(grpc_ctxtags.CodeGenRequestFieldExtractor)),
				grpc_logrus.StreamServerInterceptor(entry, opts...),
			))...,
	)

	entry.WithFields(logrus.Fields{
		"service-info": gs.GetServiceInfo(),
	}).Info("Service information")

	return gs
}

// ReadCertificatesForServer will read certs and return an gRPC server option.
func ReadCertificatesForServer(certFile, keyFile string) (grpc.ServerOption, error) {
	// Read cert and key file - would normally get from a secret store
	backendCert, err := ioutil.ReadFile(certFile)
	if err != nil {
		return nil, err
	}

	backendKey, err := ioutil.ReadFile(keyFile)
	if err != nil {
		return nil, err
	}

	// Generate Certificate struct
	cert, err := tls.X509KeyPair(backendCert, backendKey)
	if err != nil {
		return nil, err
	}

	// Create credentials and use Credentials in gRPC server options
	return grpc.Creds(credentials.NewServerTLSFromCert(&cert)), nil
}

// ReadCertificatesForDialer will read certs and return an gRPC dial option.
func ReadCertificatesForDialer(certFile string) (grpc.DialOption, error) {
	// Read cert file
	cert, err := ioutil.ReadFile(certFile)
	if err != nil {
		return nil, err
	}

	// Create CertPool
	roots := x509.NewCertPool()
	if ok := roots.AppendCertsFromPEM(cert); !ok {
		return nil, errors.New("could not add cert to pool")
	}

	// Create credentials and use Credentials in gRPC dial options
	return grpc.WithTransportCredentials(credentials.NewClientTLSFromCert(roots, "")), nil
}
