package odcr

import (
	"github.com/grpc-ecosystem/go-grpc-middleware/logging/logrus"
	"github.com/sirupsen/logrus"
	"os"
)

// NewLogger return a new logrus logger abd the base entry required for
// chain interceptors in the gRPC lib. That lib should use the interface logrus.FieldLogger.
func NewLogger(name string, debug bool) (*logrus.Logger, *logrus.Entry) {
	logger := logrus.New()

	// Log as JSON instead of the default ASCII formatter.
	logger.Formatter = &logrus.JSONFormatter{}

	// Output to stdout instead of the default stderr
	// Can be any io.Writer, see below for File example
	logger.Out = os.Stdout

	if debug {
		logger.SetLevel(logrus.DebugLevel)
	} else {
		// Only log the warning severity or above.
		logger.SetLevel(logrus.WarnLevel)
	}

	entry := logger.WithFields(logrus.Fields{
		"service": name,
	})

	grpc_logrus.ReplaceGrpcLogger(entry)

	return logger, entry
}
