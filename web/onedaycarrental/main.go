package main

import (
	"bitbucket.org/twinj/odcr/lib/odcr"
	"bitbucket.org/twinj/odcr/usvc/rentalcompanya/protobuf/rca"
	"bitbucket.org/twinj/odcr/usvc/rentalcompanyb/protobuf/rcb"
	"bitbucket.org/twinj/odcr/web/onedaycarrental/lib"
	"context"
	"flag"
	"fmt"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"google.golang.org/grpc"
	"log"
	"net"
	"net/http"
	"os"
)

const (
	name = "onedaycarrental"
)

var (
	port = "8080"
	tls  bool

	lbAddr   = "localhost:1443"
	certFile string
)

func init() {
	flag.BoolVar(&tls, "tls", true, "Connection uses TLS if true, else plain TCP")
	certFile = os.Getenv("SSL_CERT")
	if env, ok := os.LookupEnv("SRV_URL"); ok {
		lbAddr = env
	}
	flag.Parse()
}

func main() {
	var (
		opt = grpc.WithInsecure()
		err error
	)

	if tls {
		opt, err = odcr.ReadCertificatesForDialer(certFile)
		if err != nil {
			log.Fatalf("failed to get credentials: %v", err)
		}
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	connA, err := grpc.DialContext(ctx, lbAddr, opt)
	if err != nil {
		log.Fatalf("failed to dial rental company a: %v", err)
	}
	defer connA.Close()

	connB, err := grpc.DialContext(ctx, lbAddr, opt)
	if err != nil {
		log.Fatalf("failed to dial rental company b: %v", err)
	}
	defer connB.Close()

	_, entry := odcr.NewLogger(name, true)

	// Create the quote collector
	quoteMeCollector := lib.GetQuotesCollectorFactory(
		entry,
		rca.NewQuoterServiceClient(connA),
		rcb.NewQuoterServiceClient(connB),
	)
	if err != nil {
		log.Fatalf("failed to create web clients service: %v", err)
	}

	lis, err := net.Listen("tcp", fmt.Sprintf(":%s", port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	// Set up handler
	handler := mux.NewRouter()
	lib.ComposeRoutes(handler, quoteMeCollector, templates, entry)
	handler.Use(handlers.RecoveryHandler(handlers.PrintRecoveryStack(true)))

	if err := http.Serve(lis, handlers.LoggingHandler(os.Stdout, handler)); err != nil {
		log.Fatalf("failed to serve rental company b: %v", err)
	}
}
