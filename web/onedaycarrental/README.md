# onedaycarrental

The service is a gateway which calls `rentalcompanya` and `rentalcompanyb` via a proxy load balancer build for handling gRPC L7 communication. 
As mentioned here [https://grpc.io/blog/loadbalancing]

To do this a mux web router is created which collects a list of `quoter` services when they are dialed and a keeps 
reference to them to handle any requests coming in. These clients are then called and the results are collated 
and returned to the user.

### Build

To do a normal build use either `go` or `make`
```bash
go build -v -tags=debug
```

```bash
make build
```

There is one build tags available using the -tags option in go build.

- `debug` this will turn on debug logging for gateway marshalling.

### Cross compile for docker
```makefile
make build-linux
make build-docker

```

### Run

By default this service will run on port `8080` with tls enabled. If running locally remember to generate a certificate 
with the right name.

### Command args

```bash
onedaycarrental -tls=false
```

### ENV VARS

SSL_CERT: sets the cert filename location to use - required when `-tls=true`
SRV_URL: sets the load balancer to connect to defaults to `localhost:1443`

Typically the certificate files you would use are at odcr/cert
