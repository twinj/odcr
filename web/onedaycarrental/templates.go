package main

import (
	"html/template"
	"log"
	"os"
	"path"
)

var (
	templates *template.Template
)

func init() {
	// Note this must be run from the same directory as the code to parse the templates - templates are provided just
	// to show the work in a nicer way
	dir, err := os.Getwd()
	if err != nil {
		log.Fatalf("failed to get working dir: %v", err)
	}

	templates, err = template.ParseGlob(path.Join(dir, "templates/*.html"))
	if err != nil {
		log.Fatalf("failed to parse templates: %v", err)
	}
}
