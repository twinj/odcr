package lib

import (
	"bitbucket.org/twinj/odcr/protobuf/ptypes/rentals"
	"context"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"html/template"
	"net/http"
)

type quotes struct {
	Quotes []*rentals.Quote `json:"quotes"`
}

// ComposeRoutes will attach all route endpoints to the handler
func ComposeRoutes(handler *mux.Router, getQuotes GetQuotesCollector, templates *template.Template, logger logrus.FieldLogger) {
	handler.Methods(http.MethodGet).Name("GetQuotes").Path("/quotes").
		HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

			qs, err := getQuotes(context.Background())
			if err != nil {
				bizErr := err.(Error)
				http.Error(w, fmt.Sprintf("There was issue getting quotes: %v", bizErr), bizErr.Code)
				return
			}

			// Execute the html template for human friendly viewing
			if err = templates.ExecuteTemplate(w, "quotes.html", &quotes{
				Quotes: qs,
			}); err != nil {
				http.Error(w, fmt.Sprintf("There was an issue executing your request: %v", err), http.StatusInternalServerError)
				return
			}
		})

	handler.Methods(http.MethodGet).Name("GetQuotesJSON").Path("/quotes/json").
		HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

			qs, err := getQuotes(context.Background())
			if err != nil {
				bizErr := err.(Error)
				http.Error(w, fmt.Sprintf("There was issue getting quotes: %v", bizErr), bizErr.Code)
				return
			}

			// Encode response into friendly JSON
			e := json.NewEncoder(w)
			e.SetIndent("  ", "    ")
			if err = e.Encode(&quotes{
				Quotes: qs,
			}); err != nil {
				http.Error(w, fmt.Sprintf("There was issue getting quotes: %v", err), http.StatusInternalServerError)
			}

			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusOK)
		})

	// Gimme some info on the endpoints added.
	printRoutes(handler, logger)
}

func printRoutes(handler *mux.Router, logger logrus.FieldLogger) {
	// Gimme some info on the endpoints added.
	handler.Walk(func(route *mux.Route, router *mux.Router, ancestors []*mux.Route) error {
		p, err := route.GetPathTemplate()
		if err != nil {
			return err
		}

		methods, err := route.GetMethods()
		if err != nil {
			return err
		}

		logger.Infof("%s - %s %s", route.GetName(), methods, p)
		return nil
	})
}
