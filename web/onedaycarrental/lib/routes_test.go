package lib

import (
	"bitbucket.org/twinj/odcr/protobuf/ptypes/rentals"
	"context"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"testing"
)

func TestComposeRoutes(t *testing.T) {
	r := mux.NewRouter()
	ComposeRoutes(r, func(context.Context) ([]*rentals.Quote, error) {
		return []*rentals.Quote{
			{Amount: 35, Name: "Service Rental A", Currency: "USD"},
			{Amount: 45.5, Name: "Service Rental B", Currency: "AUD"},
		}, nil
	}, nil, logrus.New())

	r1 := r.GetRoute("GetQuotes")
	if r1 == nil {
		t.Error("GetQuotes route should exist")
	}

	r2 := r.GetRoute("GetQuotesJSON")
	if r2 == nil {
		t.Error("GetQuotesJSON route should exist")
	}
}
