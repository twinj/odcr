package lib

import (
	"bitbucket.org/twinj/odcr/lib/service"
	"bitbucket.org/twinj/odcr/protobuf/ptypes/rentals"
	"context"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/sirupsen/logrus"
	"net/http"
)

// Error handles http errors and codes.
type Error struct {
	Code    int
	Message string
	err     error
}

// Error satisfies the error interface.
func (e Error) Error() string {
	return e.Message
}

// GetQuotesCollector should get quotes from each service and collect them into a list
type GetQuotesCollector func(context.Context) ([]*rentals.Quote, error)

// GetQuotesCollectorFactory returns a new GetQuotesCollector.
func GetQuotesCollectorFactory(logger logrus.FieldLogger, clients ...service.QuoterServiceClient) GetQuotesCollector {
	logger.Infof("Creating get quotes collector for %d quoter clients", len(clients))
	return func(ctx context.Context) ([]*rentals.Quote, error) {
		var (
			quotes = make([]*rentals.Quote, len(clients))
			err    error
		)

		for i, c := range clients {
			quotes[i], err = c.GetQuote(ctx, &empty.Empty{})
			if err != nil {
				return nil, Error{
					Code:    http.StatusInternalServerError,
					Message: err.Error(),
				}
			}
		}

		return quotes, nil
	}
}
